resource "google_compute_disk" "mongodb_disks" {
  count = "${var.replica_set_size}"
  name  = "mongo-data-${count.index}"
  type  = "pd-ssd"
  zone  = "us-west1-a"
  size  = "${var.disk_size}"
  labels {
    environment = "${var.environment}"
  }
}
