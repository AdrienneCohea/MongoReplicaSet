#!/bin/bash

set -e

# Determine commit hash
if [ -z ${CI_COMMIT_SHA} ];
then
    COMMIT_HASH=$(git rev-parse HEAD)
else
    COMMIT_HASH=${CI_COMMIT_SHA}
fi

terraform init

terraform destroy -auto-approve \
    -var "commit_hash=${COMMIT_HASH}"
