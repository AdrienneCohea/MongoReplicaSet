provider "google" {
  credentials = "${file("~/.gcloud/adrienne-devops-f65ba858de7b.json")}"
  project     = "adrienne-devops"
  region      = "us-west1"
}
