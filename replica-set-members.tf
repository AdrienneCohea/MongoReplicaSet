resource "google_compute_instance" "mongodb_replica_set_members" {
  count = "${var.replica_set_size}"
  name  = "mongo-${count.index}"
  machine_type = "n1-standard-1"
  zone         = "us-west1-a"

  boot_disk {
    initialize_params {
      image = "mongo-${var.commit_hash}"
    }
  }

  attached_disk {
      source = "${element(google_compute_disk.mongodb_disks.*.self_link, count.index)}"
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}