#!/bin/bash

# Seems to not care if it runs multiple times
sudo sgdisk /dev/sdb

# Returns 4 if partition already created
sudo sgdisk --largest-new=1 /dev/sdb || true

# Returns 1 if partition already formatted
sudo mkfs.xfs /dev/sdb1 || true

# Returns 32 if already mounted
sudo mount /dev/sdb1 /var/lib/mongodb || true

# Assure MongoDB has rights to access
sudo chown mongodb:mongodb /var/lib/mongodb

# Start MongoDB
sudo systemctl restart mongod
