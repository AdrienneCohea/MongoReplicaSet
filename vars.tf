variable "replica_set_size" {
    default = 3
}

# Size of each disk in GB.
variable "disk_size" {
    default = 20
}

variable "environment" {
    default = "staging"
}

variable "commit_hash" {}
